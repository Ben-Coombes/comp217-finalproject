// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnWallGun.h"

// Sets default values for this component's properties
USpawnWallGun::USpawnWallGun()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USpawnWallGun::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void USpawnWallGun::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USpawnWallGun::SpawnGun(FVector spawnLocation, FRotator rotation)
{
	const FActorSpawnParameters spawnParams;
	GetWorld()->SpawnActor<AActor>(ActorToSpawn, spawnLocation, rotation, spawnParams);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Spawned Gun"));
}

