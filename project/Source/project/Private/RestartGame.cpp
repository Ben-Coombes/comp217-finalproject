// Fill out your copyright notice in the Description page of Project Settings.


#include "RestartGame.h"
#include <Editor/LevelEditor/Public/LevelEditorActions.h>
#include "Kismet/GameplayStatics.h"

// Sets default values
ARestartGame::ARestartGame()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Component"));
	//TriggerComponent->SetupAttachment(MeshComponent);
}

// Called when the game starts or when spawned
void ARestartGame::BeginPlay()
{
	Super::BeginPlay();
	TriggerComponent->OnComponentBeginOverlap.AddDynamic(this, &ARestartGame::TriggerBeginOverlap);
}

// Called every frame
void ARestartGame::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARestartGame::TriggerBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("This is an on screen message!"));
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
	




	
}

