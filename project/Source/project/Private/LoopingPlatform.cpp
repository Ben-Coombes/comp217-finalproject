// Fill out your copyright notice in the Description page of Project Settings.


#include "LoopingPlatform.h"

// Sets default values
ALoopingPlatform::ALoopingPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("Spline Component"));
	SplineComponent->SetupAttachment(GetRootComponent());

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	MeshComponent->SetupAttachment(SplineComponent);

	
}

// Called when the game starts or when spawned
void ALoopingPlatform::BeginPlay()
{
	Super::BeginPlay();

	

	const FVector SplineLocation = SplineComponent->GetLocationAtSplinePoint(0, ESplineCoordinateSpace::World);
	MeshComponent->SetWorldLocation(SplineLocation);


	FOnTimelineFloat ProgressFunction;
	ProgressFunction.BindUFunction(this, TEXT("ProcessMovementTimeline"));
	MovementTimeline.AddInterpFloat(MovementCurve, ProgressFunction);

	FOnTimelineEvent OnTimelineFinishedFunction;
	OnTimelineFinishedFunction.BindUFunction(this, TEXT("OnEndMovementTimeline"));
	MovementTimeline.SetTimelineFinishedFunc(OnTimelineFinishedFunction);

	MovementTimeline.SetTimelineLengthMode(TL_LastKeyFrame);

	if (autoActive) {
		isReversing = false;
		MovementTimeline.Play();
	}
	
}

// Called every frame
void ALoopingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (MovementTimeline.IsPlaying()) {
		MovementTimeline.TickTimeline(DeltaTime);
	}
}

void ALoopingPlatform::ProcessMovementTimeline(float Value) {
	const float SplineLength = SplineComponent->GetSplineLength();

	FVector CurrentSplineLocation = SplineComponent->GetLocationAtDistanceAlongSpline(Value * SplineLength, ESplineCoordinateSpace::World);
	FRotator CurrentSplineRotation = SplineComponent->GetRotationAtDistanceAlongSpline(Value * SplineLength, ESplineCoordinateSpace::World);

	CurrentSplineRotation.Pitch = 0.f;
	MeshComponent->SetWorldLocationAndRotation(CurrentSplineLocation, CurrentSplineRotation);
}

void ALoopingPlatform::OnEndMovementTimeline() {
	

		if (!isReversing) {
			MovementTimeline.ReverseFromEnd();
			isReversing = true;
		}
		else if (isReversing) {
			MovementTimeline.PlayFromStart();
			isReversing = false;
		}


		//else {
			//MovementTimeline.PlayFromStart();
		//}
	
}