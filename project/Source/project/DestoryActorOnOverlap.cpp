// Fill out your copyright notice in the Description page of Project Settings.


#include "DestoryActorOnOverlap.h"
#include "DrawDebugHelpers.h"
#include "Components/SphereComponent.h"

ADestoryActorOnOverlap::ADestoryActorOnOverlap()
{
	MySphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("My Sphere Component"));
	MySphereComponent->InitSphereRadius(100.f);
	MySphereComponent->SetCollisionProfileName(TEXT("Trigger"));
	RootComponent = MySphereComponent;

	MySphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ADestoryActorOnOverlap::OnOverlapBegin);
}

void ADestoryActorOnOverlap::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Destroy();
}
